package com.burlaza;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Scanner appScanner = new Scanner(System.in);

        String firstName;
        String lastName;
        int englishGrade;
        int mathGrade;
        int scienceGrade;
        int gradeAve;

        System.out.println("What is your first name?");
        firstName = appScanner.nextLine().trim();

        System.out.println("What is your last name?");
        lastName = appScanner.nextLine().trim();

        System.out.println("What is your English grade?");
        englishGrade = appScanner.nextInt();

        System.out.println("What is your Mathematics grade?");
        mathGrade = appScanner.nextInt();

        System.out.println("What is your Science grade?");
        scienceGrade = appScanner.nextInt();

        gradeAve = (englishGrade + mathGrade + scienceGrade) / 3;

        System.out.println("Hello, " + firstName + " " + lastName + ". Your grade average is " + gradeAve);


    }
}
